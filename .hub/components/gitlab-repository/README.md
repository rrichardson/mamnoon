# Manage Gitlab Repos on behalf of the user

This component can create a gitlab repository on behalf of the user

## Input parameters

- Gitlab OAUth token as `component.gitlab.token`
- Gitlab user/org as `component.gitlab.repository.organization`
- Gitlab repository as `component.gitlab.repository.name`

